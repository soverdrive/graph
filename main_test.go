package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	exitCode := m.Run()

	os.Exit(exitCode)
}

func Test_InitNode(t *testing.T) {
	ngraph := NewGraph()

	initNode := ngraph.InitNode("main")
	assert.Equalf(t, "main", initNode.Label, "check label")
	assert.Lenf(t, ngraph.nodes, 1, "len initial node")
	assert.Lenf(t, ngraph.edges, 0, "len initial edges")
}

func Test_AddNode(t *testing.T) {
	ngraph := NewGraph()

	initNode := ngraph.InitNode("main")

	newNode := ngraph.AddNode("kedua", 0.1, initNode)
	assert.Equalf(t, "kedua", newNode.Label, "new node")
	assert.Lenf(t, ngraph.nodes, 2, "len new node")
	assert.Lenf(t, ngraph.edges, 1, "len new edge")

	// trying to trace back new node to initial node
	var tracingCheck bool
	for _, oneEdge := range ngraph.edges {
		targetConfirmed := oneEdge.targetNode.ID == newNode.ID
		sourceConfirmed := oneEdge.sourceNode.ID == initNode.ID
		if targetConfirmed && sourceConfirmed {
			tracingCheck = true
		}
	}

	if !tracingCheck {
		t.Errorf("tracing does not check")
	}
}

func Test_AddEdge(t *testing.T) {
	ngraph := NewGraph()

	initNode := ngraph.InitNode("main")

	secondNode := ngraph.AddNode("kedua", 0.1, initNode)
	assert.Equalf(t, "kedua", secondNode.Label, "second node")
	assert.Lenf(t, ngraph.nodes, 2, "len second node")
	assert.Lenf(t, ngraph.edges, 1, "len first edge")

	ngraph.AddEdge(initNode, secondNode, 1.5)
	assert.Lenf(t, ngraph.edges, 2, "len second edge")

	// trying to trace two edges
	var tracingCheck int
	for _, oneEdge := range ngraph.edges {
		targetConfirmed := oneEdge.targetNode.ID == secondNode.ID
		sourceConfirmed := oneEdge.sourceNode.ID == initNode.ID
		if targetConfirmed && sourceConfirmed {
			tracingCheck++
		}
	}

	if tracingCheck < 2 {
		t.Errorf("tracing does not check")
	}
}
