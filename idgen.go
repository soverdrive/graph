package main

import "sync/atomic"

type IdentifierInteger struct {
	id int64
}

func (idint *IdentifierInteger) NewID() int64 {
	atomic.AddInt64(&idint.id, int64(1))
	return idint.CurrentID()
}

func (idint *IdentifierInteger) CurrentID() int64 {
	return atomic.LoadInt64(&idint.id)
}

type IdentifierGenerator interface {
	NewID() int64
	CurrentID() int64
}
