package main

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_IdentifierInteger(t *testing.T) {
	idGen := &IdentifierInteger{}

	assert.Equalf(t, int64(0), idGen.CurrentID(), "initial value")

	firstID := idGen.NewID()
	assert.Equalf(t, int64(1), firstID, "first next id")
	assert.Equalf(t, int64(1), idGen.CurrentID(), "check with internal var")

	rng := rand.NewSource(time.Now().Unix())
	limit := rng.Int63()%100 + 1
	for i := firstID; i < limit; i++ {
		go idGen.NewID()
	}

	time.Sleep(100 * time.Millisecond)

	expectedFinalID := firstID + limit
	finalID := idGen.NewID()

	assert.Equalf(t, expectedFinalID, finalID, "final id check")
	assert.Equalf(t, expectedFinalID, idGen.CurrentID(), "final check with internal var")
}
