package main

import (
	"fmt"
	"os"
	"sync"
)

type Graph struct {
	isDirected bool
	lock       *sync.Mutex

	nodes          []*GraphNode
	nodeIdentifier IdentifierGenerator

	edges          []*GraphEdge
	edgeIdenfifier IdentifierGenerator
}

func NewGraph() *Graph {
	return &Graph{
		nodeIdentifier: &IdentifierInteger{},
		edgeIdenfifier: &IdentifierInteger{},
		isDirected:     true,
		lock:           &sync.Mutex{},
	}
}

func (g *Graph) SetIdentifierGenerator(nodeIDGen IdentifierGenerator,
	edgeIDGen IdentifierGenerator) {
	g.lock.Lock()
	defer g.lock.Unlock()
	if len(g.nodes) > 0 || len(g.edges) > 0 {
		return
	}

	g.nodeIdentifier = nodeIDGen
	g.edgeIdenfifier = edgeIDGen
}

func (g *Graph) InitNode(nodeLabel string) *GraphNode {
	newNode := &GraphNode{
		ID:    g.nodeIdentifier.NewID(),
		Label: nodeLabel,
	}

	g.nodes = append(g.nodes, newNode)

	return newNode
}

func (g *Graph) AddNode(nodeLabel string, edgeValue float64,
	sourceNode *GraphNode) *GraphNode {

	newNode := &GraphNode{
		ID:    g.nodeIdentifier.NewID(),
		Label: nodeLabel,
	}

	newEdge := &GraphEdge{
		ID:         g.edgeIdenfifier.NewID(),
		Value:      edgeValue,
		targetNode: newNode,
		sourceNode: sourceNode,
	}

	newNode.inEdge = append(newNode.inEdge, newEdge)
	sourceNode.outEdge = append(sourceNode.outEdge, newEdge)

	g.nodes = append(g.nodes, newNode)
	g.edges = append(g.edges, newEdge)

	return newNode
}

func (g *Graph) AddEdge(sourceNode, targetNode *GraphNode,
	edgeValue float64) *GraphEdge {

	newEdge := &GraphEdge{
		ID:         g.edgeIdenfifier.NewID(),
		Value:      edgeValue,
		targetNode: targetNode,
		sourceNode: sourceNode,
	}

	targetNode.inEdge = append(targetNode.inEdge, newEdge)
	sourceNode.outEdge = append(sourceNode.outEdge, newEdge)

	g.edges = append(g.edges, newEdge)

	return newEdge
}

type GraphNode struct {
	ID      int64
	Label   string
	inEdge  []*GraphEdge
	outEdge []*GraphEdge
}

type GraphEdge struct {
	ID         int64
	Value      float64
	targetNode *GraphNode
	sourceNode *GraphNode
}

func main() {
	fmt.Fprintf(os.Stdout, "this is a projet for graph related work")
}
